package com.itmck.springboot.core.annotation;


import com.itmck.springboot.core.enums.DataSourceType;

import java.lang.annotation.*;

/**
 * 自定义多数据源切换注解
 *
 * @author miaochangke
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Ds {
    /**
     * 切换数据源名称
     */
    DataSourceType value() default DataSourceType.MASTER;
}