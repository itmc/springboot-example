package com.itmck.springboot.project.vo;

import com.itmck.springboot.common.bo.PageBodyRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/15 16:14
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@ApiModel(value = "用户类")
public class UserDTO extends PageBodyRequest {

    private Long userId;

    @ApiModelProperty(value = "用户名", name = "userName", example = "mck")
    private String userName;

    private String password;

    private String status;


}