package com.itmck.springboot.project.aop;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;
import com.itmck.springboot.project.annotation.Decrypt;
import com.itmck.springboot.project.annotation.FieldDecrypt;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Arrays;

@Slf4j
@Aspect
@Component
public class EncryptAop {


    /**
     * 定义切点：切点表达式的意思:扫描com.itmck.springboot.project.controller包下所有的类中所有的方法
     */
    @Pointcut("execution(public * com.itmck.springboot.project.controller.*.*(..))")
    public void des() {
    }


    @Before("des()")
    public void before(JoinPoint joinPoint) {
        //获取参数列表
        Object[] args = joinPoint.getArgs();
        if (CollectionUtil.isNotEmpty(Arrays.asList(args))) {
            extracted(args[0]);
        }
    }

    private static void extracted(Object obj) {
        if (obj.getClass().isAnnotationPresent(Decrypt.class)) {
            //如果类上有解密注解。再去判断是否存在字段上标记注解
            Field[] fields = ReflectUtil.getFields(obj.getClass());
            for (Field field : fields) {
                Object fieldValue1 = ReflectUtil.getFieldValue(obj, field);
                System.out.println(fieldValue1);
                if (field.isAnnotationPresent(FieldDecrypt.class)) {
                    if (field.getType().equals(String.class)) {
                        String fieldValue = (String) ReflectUtil.getFieldValue(obj, field);
                        //这里使用--代表解密
                        ReflectUtil.setFieldValue(obj, field, fieldValue + "--");
                    }
                }
            }
        }
    }
}