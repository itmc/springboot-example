package com.itmck.springboot.project.config;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;


/**
 * 这里顺便实现了
 * 实现ApplicationContextAware:目的是为了拿到ApplicationContext容器
 * 实现ApplicationRunner: 该接口中只有一个run方法，他执行的时机是：spring容器启动完成之后，就会紧接着执行这个接口实现类的run方法。
 */
@Slf4j
@Configuration
@EnableSwagger2WebMvc
public class Knife4jConfiguration implements ApplicationRunner, ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("针对springboot项目创建的基础学习框架")
                        .description("包含常用springboot集成框架")
                        .termsOfServiceUrl("http://www.xx.com/")
                        .contact(new Contact("mck", "111", "347451331@qq.com"))
                        .version("1.0")
                        .build())
                //分组名称
                .groupName("2.X版本")
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.itmck.springboot.project.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void run(ApplicationArguments args) {
        Environment environment = applicationContext.getEnvironment();

        String appName = applicationContext.getEnvironment().getProperty("spring.application.name");
        String contextPath = Optional.ofNullable(applicationContext.getEnvironment().getProperty("server.servlet.context-path")).orElse("");
        try {
            String host = InetAddress.getLocalHost().getHostAddress();
            String port = Optional.ofNullable(applicationContext.getEnvironment().getProperty("server.port")).orElse("8080");
            String profile = applicationContext.getEnvironment().getProperty("spring.profiles.active");
            log.info("\n----------------------------------------------------------\n\t --Application: '{}' is running! \n\t Environment:  {} \n\t Swagger Doc:  http://{}:{}{}/doc.html \n----------------------------------------------------------", appName, profile, host, port, contextPath);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

}