package com.itmck.springboot.project.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


/**
 * 事件发布者，通过 注入ApplicationEventPublisher 进行事件发布
 */
@Slf4j
@Component
public class MessageEventPublisher {

    @Resource
    private ApplicationEventPublisher publisher;

    /**
     * 发布事件
     *
     * @param user
     */
    @Async
    public void publish(String message) {
        log.info("发布事件:{}", message);
        this.publisher.publishEvent(new MessageEvent(this, message));
    }

}