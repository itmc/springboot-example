package com.itmck.springboot.project.controller;


import com.alibaba.fastjson.JSON;
import com.itmck.springboot.common.bo.ApiResponse;
import com.itmck.springboot.common.bo.PageBodyResponse;
import com.itmck.springboot.project.vo.AopDTO;
import com.itmck.springboot.project.vo.UserDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/aop")
@Api(value = "AOPController", tags = "aop")
public class AOPController {

    @ApiOperation(value = "测试aop前置通知", notes = "测试aop前置通知")
    @PostMapping("/hello")
    public ApiResponse<AopDTO> hello(@RequestBody AopDTO AopDTO) {
        log.info("请求参数:{}", JSON.toJSONString(AopDTO));
        return ApiResponse.ok(AopDTO);
    }

}
