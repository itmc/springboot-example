package com.itmck.springboot.project.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itmck.springboot.common.bo.PageBodyResponse;
import com.itmck.springboot.core.annotation.Ds;
import com.itmck.springboot.core.enums.DataSourceType;
import com.itmck.springboot.project.dao.entity.Student;
import com.itmck.springboot.project.dao.entity.User;
import com.itmck.springboot.project.dao.mapper.StudentMapper;
import com.itmck.springboot.project.dao.mapper.UserMapper;
import com.itmck.springboot.project.service.UserService;
import com.itmck.springboot.project.util.PageUtil;
import com.itmck.springboot.project.vo.UserDTO;
import com.itmck.springboot.project.vo.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private StudentMapper studentMapper;

    @Override
    public PageBodyResponse<UserDTO> pages(UserDTO userDTO) {
        //单表
        Page<User> pageBody = this.lambdaQuery()
                .eq(User::getUserName, userDTO.getUserName())
                .page(new Page<>(userDTO.getCurrentPage(), userDTO.getPageSize()));
        return PageUtil.covertRecordType(pageBody, UserDTO.class);
    }


    @Override
    public PageBodyResponse<UserDTO> mulPages(UserDTO userDTO) {
        //多表分页，集成mybatis-plus之后，多表分页可以简化如下：
        Page<UserVO> page = new Page<>(userDTO.getCurrentPage(), userDTO.getPageSize());
        Page<UserVO> pageBody = userMapper.mulTablePage(page, userDTO);
        return PageUtil.covertRecordType(pageBody, UserDTO.class);
    }


    @Override
    public List<Student> slaveDb() {
        return studentMapper.slaveDb();
    }
}
