package com.itmck.springboot.project.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MessageEventListener {


    @EventListener(MessageEvent.class)
    public void processEvent(MessageEvent orderEvent) {
        log.info("监听数据:{}", orderEvent.getMessage());
    }
}