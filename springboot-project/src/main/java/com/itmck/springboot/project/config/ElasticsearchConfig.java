//package com.itmck.springboot.project.config;
//
//import org.apache.http.HttpHost;
//import org.elasticsearch.client.RestClient;
//import org.elasticsearch.client.RestHighLevelClient;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//
///**
// * es配置客户端
// * <p>
// * Create by M ChangKe 2023/9/3 12:57
// */
//@Configuration
//public class ElasticsearchConfig {
//
//    @Bean
//    public RestHighLevelClient restHighLevelClient() {
//        return new RestHighLevelClient(
//                RestClient.builder(
//                        new HttpHost("127.0.0.1", 9200, "http")
//                )
//        );
//    }
//}
