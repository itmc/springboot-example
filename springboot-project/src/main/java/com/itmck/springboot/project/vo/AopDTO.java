package com.itmck.springboot.project.vo;


import com.itmck.springboot.project.annotation.Decrypt;
import com.itmck.springboot.project.annotation.FieldDecrypt;
import lombok.Data;

import java.io.Serializable;

@Data
@Decrypt
public class AopDTO implements Serializable {

    @FieldDecrypt
    private String certNo;
}
