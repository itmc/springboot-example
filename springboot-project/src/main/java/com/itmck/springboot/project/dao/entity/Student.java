package com.itmck.springboot.project.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/15 16:14
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "tb_user")
public class Student implements Serializable {


    private static final long serialVersionUID = -1205226416664488559L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long userId;


    @TableField(value = "user_name")
    private String userName;


    @TableField(value = "password")
    private String password;

    @TableField(value = "status")
    private String status;


    public Student(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }
}