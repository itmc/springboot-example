package com.itmck.springboot.project.controller;


import com.alibaba.fastjson.JSON;
import com.itmck.springboot.common.bo.ApiResponse;
import com.itmck.springboot.common.bo.PageBodyResponse;
import com.itmck.springboot.project.service.UserService;
import com.itmck.springboot.project.vo.UserDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("/mybatis")
@Api(value = "MybatisController", tags = "mybatis")
public class MybatisController {

    @Resource
    private UserService userService;

    @ApiOperation(value = "单表分页查询", notes = "分页查询")
    @PostMapping("pages")
    public PageBodyResponse<UserDTO> pages(@RequestBody UserDTO userDTO) {
        log.info("条件查询分页数据,请求参数:{}", JSON.toJSONString(userDTO));
        return userService.pages(userDTO);
    }


    @ApiOperation(value = "多表分页查询", notes = "分页查询")
    @PostMapping("mul")
    public PageBodyResponse<UserDTO> mulPages(@RequestBody UserDTO userDTO) {
        log.info("条件查询分页数据,请求参数:{}", JSON.toJSONString(userDTO));
        return userService.mulPages(userDTO);
    }


}
