package com.itmck.springboot.project.controller;


import cn.hutool.core.util.StrUtil;
import com.itmck.springboot.common.bo.ApiResponse;
import com.itmck.springboot.project.redis.RedisDistributedLock;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@RestController
@RequestMapping("/redis")
@Api(value = "RedisController", tags = "redis")
public class RedisController {
    private static final String lockKey = "product_001";
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Resource
    private RedissonClient redissonClient;


    @ApiOperation(value = "setNx", notes = "setNx")
    @GetMapping("/setNx")
    public ApiResponse<String> stock() {
        //创建随机数，后续使用此确定是哪个客户端
        String clientId = Thread.currentThread().getId() + UUID.randomUUID().toString();
        try {
            //使用 set key value ex 10 nx
            //设置当lockKey不存在，设置10s失效，设置成功说明拿到了锁可以进行扣减库存
            Boolean b = redisTemplate.opsForValue().setIfAbsent(lockKey, clientId, 10, TimeUnit.SECONDS);
            if (Boolean.TRUE.equals(b)) {
                //如果加锁成功
                String nums = redisTemplate.opsForValue().get("stock");
                if (StrUtil.isNotBlank(nums)) {
                    assert nums != null;
                    int stock = Integer.parseInt(nums);
                    if (stock > 0) {
                        int realStock = stock - 1;
                        redisTemplate.opsForValue().set("stock", realStock + "");
                        return ApiResponse.ok("扣减库存成功,剩余:" + realStock);
                    } else {
                        return ApiResponse.ok("库存不足");
                    }
                }
            }
            return ApiResponse.ok("稍后重试...");
        } finally {
            //这里主要保证，锁是自己释放的，不是被别的客户端释放
            if (clientId.equals(redisTemplate.opsForValue().get(lockKey))) {
                redisTemplate.delete(lockKey);
            }
        }
    }


    /**
     * 手写分布式锁
     *
     * @return
     */
    @ApiOperation(value = "RedisDistributedLock", notes = "RedisDistributedLock")
    @GetMapping("/RedisDistributedLock")
    public ApiResponse<String> redisDistributedLock() {
        //创建分布式锁
        RedisDistributedLock redisDistributedLock = new RedisDistributedLock(lockKey, redisTemplate);
        redisDistributedLock.lock();
        try {
            //如果加锁成功
            int stock = Integer.parseInt(Objects.requireNonNull(redisTemplate.opsForValue().get("stock")));
            if (stock > 0) {
                int realStock = stock - 1;
                redisTemplate.opsForValue().set("stock", realStock + "");
                return ApiResponse.ok("扣减库存成功,剩余:" + realStock);
            } else {
                return ApiResponse.ok("库存不足");
            }
        } finally {
            //解锁
            redisDistributedLock.unlock();
        }
    }

    @ApiOperation(value = "redissonClient", notes = "redissonClient")
    @GetMapping("/redissonClient")
    public ApiResponse<String> deleteProduct2() {
        //获取redis锁
        RLock lock = redissonClient.getLock(lockKey);
        //加锁,实现锁续命.默认30s
        lock.lock();
        try {
            int stock = Integer.parseInt(Objects.requireNonNull(redisTemplate.opsForValue().get("stock")));
            if (stock > 0) {
                int realStock = stock - 1;
                redisTemplate.opsForValue().set("stock", realStock + "");
                return ApiResponse.ok("扣减库存成功,剩余:" + realStock);
            } else {
                return ApiResponse.ok("库存不足");
            }
        } finally {
            //解锁
            lock.unlock();
        }
    }
}
