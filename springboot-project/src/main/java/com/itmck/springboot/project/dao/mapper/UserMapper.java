package com.itmck.springboot.project.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itmck.springboot.project.dao.entity.User;
import com.itmck.springboot.project.vo.UserDTO;
import com.itmck.springboot.project.vo.UserVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * 自定义mapper继承BaseMapper拥有crud能力
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {


    /**
     * @param page    mp识别到page会进行分页操作
     * @param userDTO 请求参数，条件
     * @return 分页后的数据
     */
    Page<UserVO> mulTablePage(Page<UserVO> page, @Param("user") UserDTO userDTO);
}
