package com.itmck.springboot.project.config;


import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@Configuration
public class RedisConfig {

    @Resource
    public RedisProperties redisProperties;

    @Bean
    public RedissonClient redissonClient() {
        String host = redisProperties.getHost();
        Config config = new Config();
        //如果是单机
        config.useSingleServer()
                .setAddress("redis://" + host + ":" + redisProperties.getPort())
                .setPassword(redisProperties.getPassword());
        return Redisson.create(config);


//        List<String> nodeAddressList = redisProperties.getCluster().getNodes();
//        //初始化需要得到的数组
//        String[] array = new String[nodeAddressList.size()];
//        for (int i = 0; i < nodeAddressList.size(); i++) {
//            array[i] = "redis://" + nodeAddressList.get(i);
//        }
//        log.info("redis集群地址:{}", Arrays.toString(array));
//        Config config = new Config();
//        config.useClusterServers()
//                .addNodeAddress(array)
//                .setPassword(redisProperties.getPassword())
//                .setScanInterval(2000);
//        return Redisson.create(config);
    }
}
