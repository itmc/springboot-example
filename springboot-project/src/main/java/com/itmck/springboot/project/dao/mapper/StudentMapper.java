package com.itmck.springboot.project.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itmck.springboot.core.annotation.Ds;
import com.itmck.springboot.core.enums.DataSourceType;
import com.itmck.springboot.project.dao.entity.Student;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * 自定义mapper继承BaseMapper拥有crud能力
 */
@Mapper
public interface StudentMapper extends BaseMapper<Student> {


    @Select(value = "select * from tb_user")
    @Ds(value = DataSourceType.SLAVE)
    List<Student> slaveDb();
}
