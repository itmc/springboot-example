package com.itmck.springboot.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itmck.springboot.common.bo.PageBodyResponse;
import com.itmck.springboot.core.annotation.Ds;
import com.itmck.springboot.core.enums.DataSourceType;
import com.itmck.springboot.project.dao.entity.Student;
import com.itmck.springboot.project.dao.entity.User;
import com.itmck.springboot.project.vo.UserDTO;

import java.util.List;

/**
 * 自定义service实现IService接口,然后 自定义Impl extends ServiceImpl 使得其拥有mp提供的通用crud能力
 */
public interface UserService extends IService<User> {

    /**
     * 单表分页查询
     *
     * @param userDTO 参数
     * @return 分页数据
     */
    PageBodyResponse<UserDTO> pages(UserDTO userDTO);

    PageBodyResponse<UserDTO> mulPages(UserDTO userDTO);


    List<Student> slaveDb();
}
