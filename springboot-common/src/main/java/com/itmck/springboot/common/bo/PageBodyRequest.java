package com.itmck.springboot.common.bo;


import lombok.Data;

@Data
public class PageBodyRequest {

    /**
     * 当前页面
     */
    long currentPage = 1;

    /**
     * 每页大小
     */
    long pageSize = 10;
}
