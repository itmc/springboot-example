## mac安装redis
1. 使用home-brew一键安装：`brew install redis`
```shell
brew install redis
```
2. 启动Redis服务：`brew services start redis` 或` redis-server /usr/local/etc/redis.conf`   
```shell
brew services start redis
```
3. 关闭Redis服务：'brew services stop redis'  
```shell
brew services stop redis
```
4. 重启Redis服务：`brew services restart redis`
```shell
brew services restart redis
```
5. 打开图形化界面：`redis-cli`
```shell
redis-cli
```

mac如果缺少brew 需要先安装
原文链接：https://blog.csdn.net/realize_dream/article/details/106227622



一般情况是这么操作的：

1、通过brew install安装应用最先是放在/usr/local/Cellar/目录下。

2、有些应用会自动创建软链接放在/usr/bin或者/usr/sbin，同时也会将整个文件夹放在/usr/local

3、可以使用brew list 软件名 确定安装位置。

比如查找安装opencv 在那个位置

可以在mac终端直接输入 brew list opencv