## Linux下安装redis

### 下载安装包
```shell
wget https://download.redis.io/releases/redis-6.2.5.tar.gz
```
![img.png](lib/pic/img.png)

### 解压到指定目录
```shell
tar -zxvf redis-6.2.5.tar.gz -C /mck/opt
```


### 编译
进入到解压好的redis-6.2.5目录下进行编译与安装
```shell
make
make install PREFIX=/usr/local/redis
```

### 将解压的redis.conf文件复制到编译后的文件夹
```shell
cp redis.conf /usr/local/redis/bin
```

### 修改conf配置文件
* daemonize yes #后台启动
* protected‐mode no #关闭保护模式，开启的话，只有本机才可以访问redis
* 需要注释掉bind 
  * #bind 127.0.0.1（bind绑定的是自己机器网卡的ip，如果有多块网卡可以配多个ip，代表允许客户 端通过机器的哪些网卡ip去访问，内网一般可以不配置bind，注释掉即可）

![img.png](lib/pic/img_2.png)

### 启动与连接
![img_1.png](lib/pic/img_1.png)

redis默认端口6379