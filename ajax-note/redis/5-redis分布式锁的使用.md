## 什么是锁

[https://zhuanlan.zhihu.com/p/98405453](https://zhuanlan.zhihu.com/p/98405453)

* 从线程是否需要对资源加锁可以分为 悲观锁 和 乐观锁
* 从资源已被锁定，线程是否阻塞可以分为 自旋锁
* 从多个线程并发访问资源，也就是 Synchronized 可以分为 无锁、偏向锁、 轻量级锁 和 重量级锁
* 从锁的公平性进行区分，可以分为公平锁 和 非公平锁
* 从根据锁是否重复获取可以分为 可重入锁 和 不可重入锁
* 从那个多个线程能否获取同一把锁分为 共享锁 和 排他锁

## 加锁的目的是什么

锁就是为了保护临界资源，加锁的目的就是为了同一时刻只允许一个线程访问临界资源

## 如何使用锁

* 单线程环境下,如果要解决资源竞争问题,可以使用synchronized等解决
* 多线程环境下我们可以使用redis,zookeeper,数据库等来解决

## 什么是分布式锁

分布式锁是指在分布式环境下针对临界资源进行并发访问控制的一种锁。他有别于JVM锁.分布式环境下相当于跨多个服务，每个服务是一个JVM。

## 分布式锁的特点

1. `互斥`:和我们本地锁一样互斥性是最基本，但是分布式锁需要保证在不同节点的不同线程的互斥
2. `可重入`:同一个节点上的同一个线程如果获取了锁之后那么也可以再次获取这个锁
3. `锁超时`:和本地锁一样支持锁超时，防止死锁
4. `高可用`:加锁和解锁需要高效，同时也需要保证高可用防止分布式锁失效，可以增加降级
5. `加锁与解锁是同一个对象`:锁不能被别的client释放，这种情况使用随机串，比如 String clientId = UUID.randomUUID()
   .toString();

## redis如何实现锁功能

### 使用redis自带命令实现分布式锁

redis中存在一条命令:`setNx key value`,如果不存在则更新,对某个资源加锁可以

```shell
setNx key value
# eg：
# 设置product:001的值为mck，当product:001不存在返回true,否则返回失败
setNx product:001 mck
```

这里有个问题，加锁了之后如果机器宕机那么这个锁就不会得到释放所以会加入过期时间，加入过期时间需要和setNx同一个原子操作，在Redis2.8之前我们需要使用Lua脚本达到我们的目的，但是redis2.8之后redis支持nx和ex操作是同一原子操作。

```shell
set key value ex 10 nx
```

常见的EX、PX、NX、XX的含义

* EXseconds – 设置键key的过期时间，单位时秒
* PXmilliseconds – 设置键key的过期时间，单位时毫秒
* NX – 只有键key不存在的时候才会设置key的值
* XX – 只有键key存在的时候才会设置key的值

## 场景示例

现在需要减库存服务,采用的是集群部署.这时候使用传统的synchronized已经不能解决(synchronized是jvm进程内的锁)
,此时就需要分布式锁来实现.保证同一时刻只有一个线程操作临界资源.

### 实现方式1

使用： `set key value ex 10 nx`命令设置分布式锁。首先他解决了原子性问题。

弊端：
从下面代码可以实现简单的分布式锁，如果在并发量较小，不是严格要求数据一致性的情况下，可以用来作为分布式锁。
假如业务执行需要15s,这个超时时间设置为10s.这时候执行10s锁被释放,新的线程进来,还是会存在超卖问题.

```java

@RestController
public class RedisController {

    private static final String lockKey = "product_001";

    //注入StringRedisTemplate作为操作redis的工具
    @Resource
    private StringRedisTemplate redisTemplate;

    @GetMapping("/setNx")
    public ApiResponse<String> stock() {
        //创建随机数，后续使用此确定是哪个客户端
        String clientId = UUID.randomUUID().toString();
        try {
            //使用 set key value ex 10 nx
            //设置当lockKey不存在，设置10s失效，设置成功说明拿到了锁可以进行扣减库存
            Boolean b = redisTemplate.opsForValue().setIfAbsent(lockKey, clientId, 10, TimeUnit.SECONDS);
            if (Boolean.TRUE.equals(b)) {
                //如果加锁成功
                int stock = Integer.parseInt(Objects.requireNonNull(redisTemplate.opsForValue().get("stock")));
                if (stock > 0) {
                    int realStock = stock - 1;
                    redisTemplate.opsForValue().set("stock", realStock + "");
                    return ApiResponse.ok("扣减库存成功,剩余:" + realStock);
                } else {
                    return ApiResponse.ok("库存不足");
                }
            }
            return ApiResponse.ok("稍后重试...");
        } finally {
            //这里主要保证，锁是自己释放的，不是被别的客户端释放
            if (clientId.equals(redisTemplate.opsForValue().get(lockKey))) {
                redisTemplate.delete(lockKey);
            }
        }
    }

}
```

### 实现方式2

从方式1可以知道其弊端就是无法控制锁的时间，时间过长影响并发效率，时间过短小于业务执行时间会产生超卖问题。

1. 我们要严格的控制不能超卖，那锁的时间必然要大于业务执行时间。
2. 我们怎样才能在第一时间当锁被释放立马被第二个客户拿到。

```java
public interface DistributedLock extends Lock {
    /**
     * 不限时间的尝试加锁
     *
     * @param leaseTime 锁的失效时间
     * @param unit      时间单位
     */
    void lock(long leaseTime, TimeUnit unit);

    /**
     * 尝试加锁，超过等待时间时返回false
     *
     * @param waitTime  等待时间
     * @param leaseTime 锁的失效时间
     * @param unit      时间单位
     * @return true/false
     */
    boolean tryLock(long waitTime, long leaseTime, TimeUnit unit);
}
```
实现分布式锁逻辑
```java
package com.itmck.springboot.project.redis;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;


@Slf4j
public class RedisDistributedLock implements DistributedLock {

    public static final int DEFAULT_WAIT_TIME = 5;
    public static final int DEFAULT_LEASE_TIME = 120;
    private static final String LOCK_PREFIX = "LOCK.";
    private static final int INFINITY = 999_999_999;
    private final String lockKey;
    private final String lockValue;
    private static final String APP_ID = UUID.randomUUID().toString();

    private final StringRedisTemplate redisTemplate;


    public RedisDistributedLock(String lockKey, StringRedisTemplate redisTemplate) {
        this.lockKey = LOCK_PREFIX + lockKey;
        // 应用ID+线程ID，保证每个线程都拥有唯一的value，防止锁误删
        this.lockValue = APP_ID + ":" + Thread.currentThread().getId();
        this.redisTemplate = redisTemplate;
    }

    /**
     * 加锁
     * 无限等待，直到获取锁成功或抛出异常
     *
     * @param leaseTime 锁的最大期限（小于等于0 则使用默认期限）
     * @param unit      时间单位
     */
    @Override
    public void lock(long leaseTime, TimeUnit unit) {
        tryLock(INFINITY, leaseTime, unit);
    }

    /**
     * 尝试加锁
     * <p>
     * 示例：
     * // 最多等待100秒，上锁以后10秒自动解锁
     * lock.tryLock(100, 10, TimeUnit.SECONDS);
     *
     * @param waitTime  等待时间 (小于0 则使用默认等待时间；等于0 则只尝试一次加锁)
     * @param leaseTime 锁的最大期限（小于等于0 则使用默认期限）
     * @param unit      时间单位
     * @return 成功返回true，超时返回false
     */
    @Override
    public boolean tryLock(long waitTime, long leaseTime, TimeUnit unit) {

        waitTime = unit.toMillis(waitTime < 0 ? DEFAULT_WAIT_TIME : waitTime);
        // 必须设置锁的自动失效时间，防止死锁
        leaseTime = leaseTime <= 0 ? TimeUnit.SECONDS.convert(DEFAULT_LEASE_TIME, unit) : leaseTime;
        long startTime = System.currentTimeMillis();
        int tryCount = 0;
        while (true) {
            // 加锁操作必须是原子的
            Boolean result = redisTemplate.opsForValue().setIfAbsent(lockKey, lockValue, 10, TimeUnit.SECONDS);
            if (result != null && result) {
                log.debug("加锁({})成功！，最大占用锁的时间为{}毫秒", lockKey, unit.toMillis(leaseTime));
                return true;
            }
            long passTime = System.currentTimeMillis() - startTime;
            log.debug("尝试加锁第({})次失败！继续尝试{}毫秒,", ++tryCount, waitTime - passTime);
            if (waitTime <= passTime) {
                return false;
            }
        }
    }

    /**
     * 加锁
     * 无限等待，直到获取锁成功或抛出异常
     */
    @Override
    public void lock() {
        tryLock(INFINITY, DEFAULT_LEASE_TIME, TimeUnit.SECONDS);
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        throw new InterruptedException();
    }

    /**
     * 尝试加锁
     * 只会尝试一次，立即返回结果
     *
     * @return true获取到锁，false获取失败
     */
    @Override
    public boolean tryLock() {
        return tryLock(0, DEFAULT_LEASE_TIME, TimeUnit.SECONDS);
    }

    /**
     * 尝试加锁（指定最长等待时间）
     *
     * @param waitTime 等待时间，超过后返回false
     * @param unit     时间单位
     * @return 成功返回true，超时返回false
     */
    @Override
    public boolean tryLock(long waitTime, @NotNull TimeUnit unit) throws InterruptedException {
        return tryLock(waitTime, DEFAULT_LEASE_TIME, unit);
    }

    @Override
    public void unlock() {
        redisTemplate.delete(lockKey);
    }

    @NotNull
    @Override
    public Condition newCondition() {
        throw new UnsupportedOperationException();
    }
}
```

使用如下
```java
@Slf4j
@RestController
public class RedisController {

    private static final String lockKey = "product_001";

    //注入StringRedisTemplate作为操作redis的工具
    @Resource
    private StringRedisTemplate redisTemplate;
    
    @ApiOperation(value = "RedisDistributedLock", notes = "RedisDistributedLock")
    @GetMapping("/RedisDistributedLock")
    public ApiResponse<String> redisDistributedLock() {
        //创建分布式锁
        RedisDistributedLock redisDistributedLock = new RedisDistributedLock(lockKey, redisTemplate);
        redisDistributedLock.lock();
        try {
            //如果加锁成功
            int stock = Integer.parseInt(Objects.requireNonNull(redisTemplate.opsForValue().get("stock")));
            if (stock > 0) {
                int realStock = stock - 1;
                redisTemplate.opsForValue().set("stock", realStock + "");
                return ApiResponse.ok("扣减库存成功,剩余:" + realStock);
            } else {
                return ApiResponse.ok("库存不足");
            }
        } finally {
            //解锁
            redisDistributedLock.unlock();
        }
    }
}

```

### 实现方式3

从2可以看出我们可以自己实现分布式锁，但是少了锁的续期，这里可以使用redisson实现分布式锁，它已经封装了一套基于Redis的分布式框架，使用起来也很简单。

#### 1.引入依赖
```xml
<!-- Redisson 实现分布式锁 -->
<dependency>
   <groupId>org.redisson</groupId>
   <artifactId>redisson</artifactId>
   <version>2.11.5</version>
</dependency>
```
#### 2.初始化
##### 单机
```java
@Configuration
public class RedisConfig {
    
    @Bean
    public RedissonClient redissonClient() {
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://127.0.0.1:6379").setPassword(null);
        return Redisson.create(config);
    }
}
```
#### redis 哨兵
需要配置redis哨兵的地址和密码
```
@Bean
public RedissonClient redissonClient(){
    Config config = new Config();
    config.useSentinelServers()
        .addSentinelAddress("redis://127.0.0.1:26379")
        .addSentinelAddress("redis://127.0.0.2:26379")
        .addSentinelAddress("redis://127.0.0.3:26379")
        .setPassword(redissonPassword);
    RedissonClient redisson = Redisson.create(config);
    return redisson;
}
```

#### redis 集群
需要配置redis集群的地址和密码，并设置集群的扫描间隔时间

```@Bean
public RedissonClient redissonClient()
{
    Config config = new Config();
    config.useClusterServers()
            // 集群状态扫描间隔时间，单位是毫秒
            .setScanInterval(2000)
            //cluster方式至少6个节点(3主3从，3主做sharding，3从用来保证主宕机后可以高可用)
            .addNodeAddress("redis://127.0.0.1:6379" )
            .addNodeAddress("redis://127.0.0.1:6380")
            .addNodeAddress("redis://127.0.0.1:6381")
            .addNodeAddress("redis://127.0.0.1:6382")
            .addNodeAddress("redis://127.0.0.1:6383")
            .addNodeAddress("redis://127.0.0.1:6384")
            .setPassword(redissonPassword);
    RedissonClient redisson = Redisson.create(config);
    return redisson;
}
```
#### 使用
```java
@Slf4j
@RestController
public class RedisController {

    private static final String lockKey = "product_001";

    //使用Redisson实现分布式锁
    @Resource
    private RedissonClient redissonClient;

    @GetMapping("/redissonClient")
    public ApiResponse<String> deleteProduct2() {
        //获取redis锁
        RLock lock = redissonClient.getLock(lockKey);
        //加锁,实现锁续命.默认30s
        lock.lock();
        try {
            int stock = Integer.parseInt(Objects.requireNonNull(redisTemplate.opsForValue().get("stock")));
            if (stock > 0) {
                int realStock = stock - 1;
                redisTemplate.opsForValue().set("stock", realStock + "");
                return ApiResponse.ok("扣减库存成功,剩余:" + realStock);
            } else {
                return ApiResponse.ok("库存不足");
            }
        } finally {
            //解锁
            lock.unlock();
        }
    }
}

```



