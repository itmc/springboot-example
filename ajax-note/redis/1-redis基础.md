## 什么是Redis

Redis是完全开源的，遵守 BSD 协议，是一个高性能的 key-value 数据库。
有以下三个特点：

* Redis支持数据的持久化，可以将内存中的数据保存在磁盘中，重启的时候可以再次加载进行使用。
* Redis不仅仅支持简单的key-value类型的数据，同时还提供list，set，zset，hash等数据结构的存储。
* Redis支持数据的备份，即master-slave模式的数据备份。

## Redis具有的优势

* 性能极高
    * Redis能读的速度是110000次/s,写的速度是81000次/s 。
* 丰富的数据类型
    * Redis支持二进制案例的 Strings, Lists, Hash, Sets 及 Ordered Sets 数据类型操作。
* 原子性
    * Redis的所有操作都是原子性的，意思就是要么成功执行要么失败完全不执行。单个操作是原子性的。多个操作也支持事务（鸡肋基本不用），即原子性，通过MULTI和EXEC指令包起来。
* 丰富的特性
    * Redis还支持 publish/subscribe, 通知, key 过期等等特性。

## Redis与其他KV存储区别

1. Redis有着更为复杂的数据结构并且提供对他们的原子性操作，这是一个不同于其他数据库的进化路径。Redis的数据类型都是基于基本数据结构的同时对程序员透明，无需进行额外的抽象。
2.

Redis运行在内存中但是可以持久化到磁盘，所以在对不同数据集进行高速读写时需要权衡内存，因为数据量不能大于硬件内存。在内存数据库方面的另一个优点是，相比在磁盘上相同的复杂的数据结构，在内存中操作起来非常简单，这样Redis可以做很多内部复杂性很强的事情。同时，在磁盘格式方面他们是紧凑的以追加的方式产生的，因为他们并不需要进行随机访问。

## redis缓存淘汰策略

* 随机:random
    * allkeys-random：从数据集（server.db[i].dict）中任意选择数据淘汰
* lru
    * volatile-lru：从已设置过期时间的数据集中挑选最近最少使用的数据淘汰。
    * allkeys-lru：从数据集中挑选最近最少使用的数据淘汰
* lfu：
    * volatile-lfu：从已设置过期时间的数据集挑选使用频率最低的数据淘汰。
    * allkeys-lfu：从数据集中挑选使用频率最低的数据淘汰。
* ttl：
    * volatile-ttl：从已设置过期时间的数据集中挑选将要过期的数据淘汰。
    * no-enviction（驱逐）：禁止驱逐数据，这也是默认策略。意思是当内存不足以容纳新入数据时，新写入操作就会报错，请求可以继续进行，线上任务也不能持续进行，采用no-enviction策略可以保证数据

这八种大体上可以分为4种，lru、lfu、random、ttl。

## Redis常见应用场景

1. 缓存（首页商品内容、购物车信息）（最多使用）
2. 任务队列（使用List）（秒杀、抢购、12306等）
3. 聊天室在线好友列表（set）
4. 应用排行榜（zset）可以进行有序排序

## Redis常用数据结构

### 字符串类型操作

```shell
#存入字符串键值对,#多次设置value会覆盖
SET  key  value  

#获取一个字符串键值
GET  key 		

#批量存储字符串键值对			
MSET key  value [key value ...]   
	
#批量获取字符串键值
MGET key  [key ...]	

#存入一个不存在的字符串键值对，如果存在则不设值并返回0
SETNX  key  value 

#过期时间为10秒，10秒后name清除（key也清除） 
setex key 10  value	

#设置一个键的过期时间（秒）
EXPIRE  key  seconds	

#删除一个键
DEL  key  [key ...] 	

#用value参数覆写给定key所储存的字符串值从偏移量offset开始
SETRANGE key offset value	

#字符串拼接
append key value	

#字符串长度
strlen key
getset	#一次性设值和读取（返回旧值，写上新值）
INCR  key	#将key中储存的数字值加1
DECR  key	#将key中储存的数字值减1
INCRBY  key  increment	#将key所储存的值加上increment
DECRBY  key  decrement 	#将key所储存的值减去decrement
setnx name mck： (not exist) #如果name不存在，则设值。如果name存在，则不设值并返回0；
setex name 10 mck:(expired) #设置name的值为mck，过期时间为10秒，10秒后name清除（key也清除）
getset name lx #一次性设值和读取(返回旧值，写上新值)
incr，decr，incrby，decrby #数值类型自增减
```

应用场景
典型应用场景：分布式锁

```shell
SET  key  value   
GET  key
SET  user:1  value(json格式数据)
MSET  user:1:name  mck user:1:balance  10
MGET  user:1:name   user:1:balance
SETNX  key value 		#返回1代表获取锁成功
SETNX   key value   #返回0代表获取锁失败
```

。。。执行业务操作

```shell
DEL   key        		#执行完业务释放锁
SET  key value  ex  10  nx  #防止程序意外终止导致死锁
```

### Hash类型操作

优点

1. 同类数据归类整合储存，方便数据管理
2. 相比string操作消耗内存与cpu更小
3. 相比string储存更节省空间
   缺点
1. 过期功能不能使用在field上，只能用在key上
2. Redis集群架构下不适合大规模使用
   操作

```shell
HSET  key  field  value	#存储一个哈希表key的键值
HGET  key  field	#获取哈希表key对应的field键值
HMSET  key  field  value [field value ...]  	#在一个哈希表key中存储多个键值对
HMGET  key  field  [field ...] 	#批量获取哈希表key中多个field键值
HDEL  key  field  [field ...]  	#删除哈希表key中的field键值
HLEN  key	#返回哈希表key中field的数量
HSETNX  key  field  value	#存储一个不存在的哈希表key的键值
HGETALL  key	#返回哈希表key中所有的键值
HINCRBY  key  field  increment 	#为哈希表key中field键的值加上增量increment
HINCRBYFLOAT key field increment 	#为哈希表key中的指定字段的浮点数值加上增量increment
HSCAN key cursor [MATCH pattern] [COUNT count]	#迭代哈希表中的键值对。
HEXISTS key field 	#查看哈希表key中指定的字段是否存在
HVALS key	#获取哈希表中所有值
HKEYS key 	#获取所有哈希表中的字段
```

### List类型操作

Redis列表是简单的字符串列表，按照插入顺序排序。你可以添加一个元素到列表的头部（左边）或者尾部（右边）

```shell

LPUSH  key  value [value ...] #将一个或多个值value插入到key列表的表头(最左边)
RPUSH  key  value [value ...]	#将一个或多个值value插入到key列表的表尾(最右边)
LPOP  key	#移除并返回key列表的头元素
RPOP  key	#移除并返回key列表的尾元素
LRANGE  key  start  stop	#返回列表key中指定区间内的元素，区间以偏移量start和stop指定
BLPOP  key  [key ...]  timeout 	#从key列表表头弹出一个元素,若列表中没有元素,阻塞等待timeout秒,如果timeout=0,一直阻塞等待
BRPOP  key  [key ...]  timeout	#从key列表表尾弹出一个元素,若列表中没有元素,阻塞等待timeout秒,如果timeout=0,一直阻塞等待
LINDEX key index	#通过索引获取列表中的元素
LINSERT key BEFORE|AFTER pivot value 	#在列表的元素前或者后插入元素
LLEN key	#获取列表长度
LREM key count value 	#移除列表元素
LSET key index value 	#通过索引设置列表元素的值
LTRIM key start stop 	#对一个列表进行修剪(trim)就是说,让列表只保留指定区间内的元素,不在指定区间之内的元素都将被删除。
RPOPLPUSH source destination 	#移除列表的最后一个元素，并将该元素添加到另一个列表并返回
RPUSHX key value 	#为已存在的列表添加值
```

典型场景就是：用作队列

### Set类型操作

Redis 的 Set 是 String 类型的无序集合。集合成员是唯一的，这就意味着集合中不能出现重复的数据。集合对象的编码可以是 intset 或者
hashtable。Redis 中集合是通过哈希表实现的，所以添加，删除，查找的复杂度都是 O(1)。

```shell
SADD key member1 [member2] 	#向集合添加一个或多个成员
SCARD key	#获取集合的成员数
SDIFF key1 [key2] 	#返回第一个集合与其他集合之间的差异
SDIFFSTORE destination key1 [key2]	#返回给定所有集合的差集并存储在destination中
SINTER key1 [key2] 	#返回给定所有集合的交集
SINTERSTORE destination key1 [key2] 	#返回给定所有集合的交集并存储在destination中
SISMEMBER key member 	#判断member元素是否是集合key的成员
SMEMBERS key	#返回集合中的所有成员
SMOVE source destination member 	#将member元素从source集合移动到destination集合
SPOP key	#移除并返回集合中的一个随机元素
SREM key member1 [member2] 	#移除集合中一个或多个成员
SUNION key1 [key2] 	#返回所有给定集合的并集
SUNIONSTORE destination key1 [key2] 	#所有给定集合的并集存储在destination集合中
SSCAN key cursor [MATCH pattern] [COUNT count] 	#迭代集合中的元素
SRANDMEMBER key [count] 	#返回集合中一个或多个随机数
```

典型场景：去重，抽奖，共同好友。

### Zset类型

Redis 有序集合和集合一样也是 string 类型元素的集合,且不允许重复的成员。不同的是每个元素都会关联一个 double 类型的分数。redis
正是通过分数来为集合中的成员进行从小到大的排序。有序集合的成员是唯一的,但分数(score)却可以重复。

```shell
ZADD key score1 member1 [score2 member2] 	#向有序集合添加一个或多个成员，或者更新已存在成员的分数
ZCARD key 	#获取有序集合的成员数
ZCOUNT key min max 	#计算在有序集合中指定区间分数的成员数
ZINCRBY key increment member 	#有序集合中对指定成员的分数加上增量increment
ZINTERSTORE destination numkeys key [key ...] 	#计算给定的一个或多个有序集的交集并将结果集存储在新的有destination中
ZLEXCOUNT key min max	#在有序集合中计算指定字典区间内成员数量
ZRANGE key start stop [WITHSCORES] 	#通过索引区间返回有序集合指定区间内的成员
ZRANGEBYLEX key min max [LIMIT offset count]	#通过字典区间返回有序集合的成员
ZRANGEBYSCORE key min max [WITHSCORES] [LIMIT] 	#通过分数返回有序集合指定区间内的成员
ZRANK key member 	#返回有序集合中指定成员的索引
ZREM key member [member ...] 	#移除有序集合中的一个或多个成员
ZREMRANGEBYLEX key min max	#移除有序集合中给定的字典区间的所有成员
ZREMRANGEBYRANK key start stop 	#移除有序集合中给定的排名区间的所有成员
ZREMRANGEBYSCORE key min max 	#移除有序集合中给定的分数区间的所有成员
ZREVRANGE key start stop [WITHSCORES]	#返回有序集中指定区间内的成员，通过索引，分数从高到低
ZREVRANGEBYSCORE key max min [WITHSCORES] 	#返回有序集中指定分数区间内的成员，分数从高到低排序
ZREVRANK key member 	#返回有序集合中指定成员的排名，有序集成员按分数值递减(从大到小)排序
ZSCORE key member	#返回有序集中，成员的分数值
ZUNIONSTORE destination numkeys key [key ...]	#计算给定的一个或多个有序集的并集，并存储在新的key中
ZSCAN key cursor [MATCH pattern] [COUNT count]	#迭代有序集合中的元素（包括元素成员和元素分值）
```

典型场景：排行榜