## Windows下安装redis

Redis安装包地址 [https://github.com/microsoftarchive/redis/releases](https://github.com/microsoftarchive/redis/releases)

解压并启动Redis服务

```shell
redis-server redis.windows.conf
```

如下所示启动成功

```shell
D:\soft\Redis-x64-3.0.504>redis-server redis.windows.conf
                _._
           _.-``__ ''-._
      _.-``    `.  `_.  ''-._           Redis 3.0.504 (00000000/0) 64 bit
  .-`` .-```.  ```\/    _.,_ ''-._
 (    '      ,       .-`  | `,    )     Running in standalone mode
 |`-._`-...-` __...-.``-._|'` _.-'|     Port: 6379
 |    `-._   `._    /     _.-'    |     PID: 12136
  `-._    `-._  `-./  _.-'    _.-'
 |`-._`-._    `-.__.-'    _.-'_.-'|
 |    `-._`-._        _.-'_.-'    |           http://redis.io
  `-._    `-._`-.__.-'_.-'    _.-'
 |`-._`-._    `-.__.-'    _.-'_.-'|
 |    `-._`-._        _.-'_.-'    |
  `-._    `-._`-.__.-'_.-'    _.-'
      `-._    `-.__.-'    _.-'
          `-._        _.-'
              `-.__.-'

[12136] 21 Mar 11:31:18.765 # Server started, Redis version 3.0.504
[12136] 21 Mar 11:31:18.766 * The server is now ready to accept connections on port 6379
```

redis默认端口6379