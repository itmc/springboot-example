# springboot整合webservice案例

> webservice现在使用的比较少,不少传统项目还在使用.现在集成测试demo供参考学习.
> webservice分为服务端和客户端.


# 引入pom依赖
```text
<!--spring boot整合CXF发布webService服务 -->
<dependency>
    <groupId>org.apache.cxf</groupId>
    <artifactId>cxf-spring-boot-starter-jaxws</artifactId>
    <version>3.2.5</version>
</dependency>
```
# 创建ws服务端
```java
package com.itmck.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService//说明当前接口为webservice
public interface RiskIcrWsService {

    @WebMethod //暴露当前方法
    String getRiskIcr(@WebParam(name = "msg") String msg);
}
```
# 实现类

```java
package com.itmck.service.impl;

import service.com.itmck.RiskIcrWsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.jws.WebService;


@Slf4j
@WebService(serviceName = "RiskIcrWsService",//对外发布的服务名
        targetNamespace = "http://impl.service.itmck.com/",//指定你想要的名称空间，通常使用使用包名反转
        endpointInterface = "com.itmck.service.RiskIcrWsService") //要发布的接口
@Component
public class RiskIcrWsServiceImpl implements RiskIcrWsService {
    @Override
    public String getRiskIcr(String msg) {

        log.info("msg:{}", msg);
        return msg;
    }
}
```
# 创建 WsConfig配置类.用于暴露出服务

```java
package com.itmck.filterconfig;

import service.com.itmck.RiskIcrWsService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.xml.ws.Endpoint;

@Configuration
public class WsConfig {

    @Resource
    private RiskIcrWsService riskIcrWsService;
    @Resource
    private Bus bus;

    /**
     * 此方法作用是改变项目中服务名的前缀名，此处127.0.0.1或者localhost不能访问时，请使用ipconfig查看本机ip来访问
     * 此方法被注释后:wsdl访问地址为http://127.0.0.1:8080/services/icr?wsdl
     * 去掉注释后：wsdl访问地址为：http://127.0.0.1:8080/hzbank/icr?wsdl
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean<CXFServlet> cxfDispatcherServlet() {
        return new ServletRegistrationBean<>(new CXFServlet(), "/hzbank/*");
    }

    /**
     * JAX-WS
     * 站点服务
     **/
    @Bean(name = "endpointRiskIcrWsService")
    public Endpoint endpointRiskIcrWsService() {
        EndpointImpl endpoint = new EndpointImpl(bus, riskIcrWsService);
        endpoint.publish("/icr");
        return endpoint;
    }

}
```
> 启动后访问 http://127.0.0.1:8080/hzbank/icr?wsdl 即可出现wsdl信息

# 生成客户端代码

- 使用jdk命令
> wsimport -s . -p com.itmck.client http://127.0.0.1:8080/hzbank/icr?wsdl
- 使用cxf命令
> wsdl2java -p com.itmck.servie -d . http://127.0.0.1:8080/hzbank/icr?wsdl

# 创建 WsClientConfig.class

```java
package com.itmck.filterconfig;

import com.itmck.client.RiskIcrWsService;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WsClientConfig {

    @Value("${webservice.icrUrl}")
    private String icrUrl;

    @Bean("riskIcrWsServiceClient")
    public RiskIcrWsService riskIcrWsServiceClient() {
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        // 设置代理地址
        jaxWsProxyFactoryBean.setAddress(icrUrl);
        // 设置接口类型
        jaxWsProxyFactoryBean.setServiceClass(RiskIcrWsService.class);
        // 创建一个代理接口实现
        return jaxWsProxyFactoryBean.create(RiskIcrWsService.class);
    }
}
```
# 创建测试类

```java
package com.itmck.service;

import com.itmck.client.RiskIcrWsService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/22 20:59
 **/
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class MyWebServiceImplTest {

    @Resource
    private RiskIcrWsService riskIcrWsService;

    @Test
    public void getRiskIcr() {

        String result = riskIcrWsService.getRiskIcr("mck");
        log.info("result:{}", result);
    }
}
```
#响应结果
>  com.itmck.service.MyWebServiceImplTest   : result:hello:mck








