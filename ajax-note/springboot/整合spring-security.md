# Spring Boot+Spring Security
> 参考:https://www.iteye.com/blog/412887952-qq-com-2441544
>参考大佬文章让你少走弯路.这位大佬写的很细

# 一、什么是SpringSecurity?
- SpringSecurity是基于Spring AOP和Servlet过滤器的安全框架。它提供全面的安全性解决方案，同时在Web 请求级和方法调用级处理身份确认和授权。在 Spring Framework 基础上，Spring Security 充分利用了依赖注入（DI，Dependency Injection）和面向切面编程（AOP）功能，为应用系统提供声明式的安全访问控制功能，减少了为企业系统安全控制编写大量重复代码的工作。
- 它是一个轻量级的安全框架，它确保基于Spring的应用程序提供身份验证和授权支持。它与Spring MVC有很好地集成，并配备了流行的安全算法实现捆绑在一起。安全主要包括两个操作“认证”与“验证”(有时候也会叫做权限控制)。“认证”是为用户建立一个其声明的角色的过程，这个角色可以一个用户、一个设备或者一个系统。“验证”指的是一个用户在你的应用中能够执行某个操作。在到达授权判断之前，角色已经在身份认证过程中建立了。

# 二、常用安全框架
  
目前常用的安全框架主要是Spring Security和Apache Shiro，它们的区别是什么呢？
  
  2.1 相同点
  
  （1）认证功能
  
  （2）授权功能
  
  （3）加密功能
  
  （4）会话功能
  
  （5）缓存支持
  
  （6）remeberMe功能
  
  ……
  
  2.2 不同点（Spring Security PK Apache Shiro）
  
  优点：
  
  （1）Spring Security基于Spring开发，项目中如果使用Spring作为基础，配合Spring Security做权限更加方便。而Shiro需要和Spring进行整合。
  
  （2）Spring Security功能比Shiro更加丰富，例如安全防护方面。
  
  （3）Spring Security社区资源相对比Shiro更加丰富。
  
  （4）如果使用的是Spring Boot，Spring Cloud的话，三者可以无缝集成。
  
  缺点：
  
  （1）Shiro的配置和使用比较简单，Spring Security上手复杂些。
  
  （2）Shiro依赖性低，不需要任何框架和容器，可以独立运行，而Spring Security依赖Spring容器。
  
企业里选择哪个安全框架，是因人而异，因团队而异，擅长哪个选择哪个，大部分的业务场景，两个框架都是可以满足需求的。


# 与springboot集成

## 引入依赖
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```
## 创建测试类
```java
package com.itmck.cotroller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {

    //localhost:8080/hello/security
    @GetMapping("/security")
    public String getWelcomeMsg() {
        return "Hello,Spring Security";
    }
}
```

## 运行后访问 localhost:8080/hello/security

![image.png](https://i.loli.net/2021/11/19/GROcDvjr2LAyiUx.png)

- 默认会使用user作为用户名,密码为一串随机串,如 Using generated security password: b3ac866a-bb11-491f-94e8-26ba1345a692
- 登陆后才能访问接口
## 创建application.yml
```yaml
spring:
  security:
    user:
      name: mck
      password: 123
```
- 此时用户名为mck.密码为123