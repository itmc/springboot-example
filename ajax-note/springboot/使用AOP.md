# springboot使用AOP

AOP即 Aspect Oriented Programming的缩写，面向切面编程。是Spring框架中一个重大的特性。
AOP主要实现的目的是针对业务处理过程中的切面进行提取，它所面对的是处理过程中的某个步骤或阶段，以获得逻辑过程中各部分之间低耦合性的隔离效果（通俗来讲，比如我们有一个功能已经上线了，现在我们想对其入参数进行修改，此时又不想在原逻辑处加代码，这时候就可以使用
aop 进行拦截处理，这种抽离原代码逻辑之外的这个动作就是面向切面编程）。
开发中最常见的可能就是日志记录，事务处理，异常处理，接口加解密处理等。

## 依赖

```xml

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-aop</artifactId>
</dependency>

```

## 分类

### 切点

@Pointcut 切入点，其中execution用于使用切面的连接点。使用方法：execution(方法修饰符(可选) 返回类型 方法名 参数 异常模式(
可选)) ，可以使用通配符匹配字符，*可以匹配任意字符。

```java
@Pointcut("execution(public * com.dalaoyang.controller.*.*(..))")
public void LogAspect(){
        }
```

### 切面

#### 前置通知

@Before 前置通知，在方法执行前执行，比如进入方法前进行参数的解密操作可以使用前置通知

```java
   //@Before 前置通知，在方法前执行
@Before("LogAspect()")
public void doBefore(JoinPoint joinPoint){
        System.out.println("doBefore");
        }

```

#### 后置通知

方法执行后在执行

```java
//@After 后置通知在方法后执行
@After("LogAspect()")
public void doAfter(JoinPoint joinPoint){

}

//@AfterReturning 在方法执行后返回一个结果后执行
@AfterReturning("LogAspect()")
public void doAfterReturning(JoinPoint joinPoint){

}

//@AfterThrowing 在方法执行过程中抛出异常的时候执行
@AfterThrowing("LogAspect()")
public void deAfterThrowing(JoinPoint joinPoint){

}
```

### 环绕通知

- 环绕通知包含了前置通知和后置通知
- @Around 环绕通知，就是可以在执行前后都使用，这个方法参数必须为ProceedingJoinPoint，proceed()

```java
@Around("LogAspect()")
public Object deAround(ProceedingJoinPoint joinPoint)throws Throwable{
    return joinPoint.proceed();
}
```

## 实战案例

现有一个场景是与第三方进行交互证件号字段进行加密传输，当外部系统请求携带加密后的证件号过来，由于我们使用的时候需要明文，那此时就可以使用aop的前置通知进行解密。

方式：使用注解+aop实现

### Decrypt注解

标注在类上

```java

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Decrypt {
}
```

### FieldDecrypt注解

标注在字段上

```java

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface FieldDecrypt {
}
```

### 创建切面

```java
package com.itmck.springboot.project.aop;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;
import com.itmck.springboot.project.annotation.Decrypt;
import com.itmck.springboot.project.annotation.FieldDecrypt;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Arrays;

@Slf4j
@Aspect//声明切面类
@Component//交给spring管理
public class EncryptAop {
    
    /**
     * 定义切点：切点表达式的意思:扫描com.itmck.springboot.project.controller包下所有的类中所有的方法
     */
    @Pointcut("execution(public * com.itmck.springboot.project.controller.*.*(..))")
    public void des() {
    }

    @Before("des()")
    public void before(JoinPoint joinPoint) {
        //获取参数列表
        Object[] args = joinPoint.getArgs();
        if (CollectionUtil.isNotEmpty(Arrays.asList(args))) {
            extracted(args[0]);
        }
    }

    private static void extracted(Object obj) {
        if (obj.getClass().isAnnotationPresent(Decrypt.class)) {
            //如果类上有解密注解。再去判断是否存在字段上标记注解
            Field[] fields = ReflectUtil.getFields(obj.getClass());
            for (Field field : fields) {
                Object fieldValue1 = ReflectUtil.getFieldValue(obj, field);
                System.out.println(fieldValue1);
                if (field.isAnnotationPresent(FieldDecrypt.class)) {
                    if (field.getType().equals(String.class)) {
                        String fieldValue = (String) ReflectUtil.getFieldValue(obj, field);
                        //这里使用--代表解密
                        ReflectUtil.setFieldValue(obj, field, fieldValue + "--");
                    }
                }
            }
        }
    }
}
```
