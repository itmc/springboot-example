## 引入swagger依赖

```xml
 <!--swagger依赖-->
<dependency>
    <groupId>com.spring4all</groupId>
    <artifactId>swagger-spring-boot-starter</artifactId>
    <version>1.9.1.RELEASE</version>
</dependency>
```

## 配置application.yml

```yaml
swagger:
  enabled: true
  base-package: com.itmck.springboot.project.controller #指定controller包
  contact:
    name: itmck
    email: 123@163.com
    url:
  title: swagger测试
  description: swagger页面
  ui-config:
    json-editor: true
```

## 启动类标记注解

使用 @EnableSwagger2开启swagger

```java

@EnableSwagger2
@SpringBootApplication
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
```

## 访问

> 地址：[localhost:8080/swagger-ui.html](localhost:8080/swagger-ui.html)